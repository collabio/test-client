//import {Client, WsClient, RtcClient, NodeRtcFactory} from 'collab.io-networking'
import { Client, WsClient, RtcClient, NodeRtcFactory, NodeHttpClient, ApiClient } from '../../../collab.io-networking';
import { LatencyTest, CorrectnessTest, FunctionalityTest } from '../../../collab.io-tests';
import { EditorController } from '../../../collab.io-client-api';
import socketio = require('socket.io');

var io = socketio.listen(process.env.port || 1338);

io.on('connection', socket => {
    let dispose;

    socket.on('test', async ({ type, server, protocol, peers }) => {
        let test;

        switch (type) {
            case 'correctness':
                test = new CorrectnessTest();
                break;
            case 'latency':
                test = new LatencyTest();
                break;
            case 'functionality':
            default:
                test = new FunctionalityTest();
                break;
        }

        switch (protocol) {
            case 'webrtc':
                await test.Begin(peers, RtcClient.Get(server + '/stun', new NodeRtcFactory()), new ApiClient(new NodeHttpClient(), server + '/api/'));
                break;
            case 'ws':
                await test.Begin(peers, WsClient.Get(server + '/relay'), new ApiClient(new NodeHttpClient(), server + '/api/'));
                break;
        }

        dispose = () => test.Dispose();
        socket.emit('results', test.Results);
    });

    socket.on('dispose', () => {
        if (dispose)
            dispose();
        dispose = null;

        RtcClient.Dispose();
        WsClient.Dispose();
    });
});