"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
//import {Client, WsClient, RtcClient, NodeRtcFactory} from 'collab.io-networking'
const collab_io_networking_1 = require("../../../collab.io-networking");
const collab_io_tests_1 = require("../../../collab.io-tests");
const socketio = require("socket.io");
var io = socketio.listen(process.env.port || 1338);
io.on('connection', socket => {
    let dispose;
    socket.on('test', ({ type, server, protocol, peers }) => __awaiter(this, void 0, void 0, function* () {
        let test;
        switch (type) {
            case 'correctness':
                test = new collab_io_tests_1.CorrectnessTest();
                break;
            case 'latency':
                test = new collab_io_tests_1.LatencyTest();
                break;
            case 'functionality':
            default:
                test = new collab_io_tests_1.FunctionalityTest();
                break;
        }
        switch (protocol) {
            case 'webrtc':
                yield test.Begin(peers, collab_io_networking_1.RtcClient.Get(server + '/stun', new collab_io_networking_1.NodeRtcFactory()), new collab_io_networking_1.ApiClient(new collab_io_networking_1.NodeHttpClient(), server + '/api/'));
                break;
            case 'ws':
                yield test.Begin(peers, collab_io_networking_1.WsClient.Get(server + '/relay'), new collab_io_networking_1.ApiClient(new collab_io_networking_1.NodeHttpClient(), server + '/api/'));
                break;
        }
        dispose = () => test.Dispose();
        socket.emit('results', test.Results);
    }));
    socket.on('dispose', () => {
        if (dispose)
            dispose();
        dispose = null;
        collab_io_networking_1.RtcClient.Dispose();
        collab_io_networking_1.WsClient.Dispose();
    });
});
